package ru.krivotulov.tm.api.controller;

public interface ITaskController {

    void displayTaskList();

    void clearTasks();

    void createTask();

}
