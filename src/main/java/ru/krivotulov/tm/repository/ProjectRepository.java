package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projectList = new ArrayList<>();

    @Override
    public Project create(final String name) {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project add(final Project project) {
        projectList.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projectList;
    }

    @Override
    public void delete(final Project project) {
        projectList.remove(project);
    }

    @Override
    public void clear() {
        projectList.clear();
    }

}
