package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.service.ITaskService;
import ru.krivotulov.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        return taskRepository.add(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void delete(final Task task) {
        if (task == null) return;
        taskRepository.delete(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
