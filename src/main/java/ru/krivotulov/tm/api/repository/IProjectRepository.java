package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    void delete(Project project);

    void clear();

}
