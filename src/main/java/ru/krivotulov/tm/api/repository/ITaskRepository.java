package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task add(Task task);

    List<Task> findAll();

    void delete(Task task);

    void clear();

}
