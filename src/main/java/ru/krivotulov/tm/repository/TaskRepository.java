package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> taskList = new ArrayList<>();

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task add(final Task task) {
        taskList.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return taskList;
    }

    @Override
    public void delete(final Task task) {
        taskList.remove(task);
    }

    @Override
    public void clear() {
        taskList.clear();
    }

}
