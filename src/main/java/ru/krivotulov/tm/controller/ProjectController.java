package ru.krivotulov.tm.controller;

import ru.krivotulov.tm.api.controller.IProjectController;
import ru.krivotulov.tm.api.service.IProjectService;
import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void displayProjectList() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projectList = projectService.findAll();
        for (Project project : projectList) {
            if (project == null) continue;
            System.out.println(project);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[PROJECT CREATE]");
        try {
            System.out.println("ENTER NAME: ");
            final String name = TerminalUtil.readLine();
            System.out.println("ENTER DESCRIPTION: ");
            final String description = TerminalUtil.readLine();
            final Project project = projectService.create(name, description);
            if (project == null) System.out.println("[FAIL]");
            else System.out.println("[OK]");
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

}
