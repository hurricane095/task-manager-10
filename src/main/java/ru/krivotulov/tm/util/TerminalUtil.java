package ru.krivotulov.tm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public interface TerminalUtil {

    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    static String readLine() throws IOException {
        return bufferedReader.readLine();
    }

}
