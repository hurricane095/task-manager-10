package ru.krivotulov.tm.api.controller;

public interface IProjectController {

    void displayProjectList();

    void clearProjects();

    void createProject();

}
