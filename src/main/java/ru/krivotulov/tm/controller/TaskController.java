package ru.krivotulov.tm.controller;

import ru.krivotulov.tm.api.controller.ITaskController;
import ru.krivotulov.tm.api.service.ITaskService;
import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void displayTaskList() {
        System.out.println("[TASK LIST]");
        final List<Task> taskList = taskService.findAll();
        for (Task task : taskList) {
            if (task == null) continue;
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[TASK CREATE]");
        try {
            System.out.println("ENTER NAME: ");
            final String name = TerminalUtil.readLine();
            System.out.println("ENTER DESCRIPTION: ");
            final String description = TerminalUtil.readLine();
            final Task task = taskService.create(name, description);
            if (task == null) System.out.println("[FAIL]");
            else System.out.println("[OK]");
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

}
