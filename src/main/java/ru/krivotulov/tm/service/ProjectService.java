package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.service.IProjectService;
import ru.krivotulov.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void delete(final Project project) {
        if (project == null) return;
        projectRepository.delete(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
